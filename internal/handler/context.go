package handler

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/spf13/viper"
	"gitlab.com/RobertArktes/profile/internal/models"
	"go.uber.org/zap"
)

//go:generate mockgen -source=context.go -destination=mocks/mock.go

type contextKey string

const (
	UsernameKey      contextKey = "username"
	ReceiversListKey contextKey = "receiversList"
	ReceiverKey      contextKey = "receiver"
)

func GetUser(ctx context.Context) (models.User, bool) {
	user, ok := ctx.Value(UsernameKey).(models.User)

	return user, ok
}

func GetReceiverList(ctx context.Context) ([]*models.Receiver, bool) {
	receiversList, ok := ctx.Value(ReceiversListKey).([]*models.Receiver)

	return receiversList, ok
}

func GetReceiver(ctx context.Context) (models.Receiver, bool) {
	receiver, ok := ctx.Value(ReceiverKey).(models.Receiver)

	return receiver, ok
}

func (h *Handler) userContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req, err := http.NewRequestWithContext(r.Context(), http.MethodGet, viper.GetString("auth.validation_address"), nil)
		if err != nil {
			zap.L().Error("Failed to make request", zap.Error(err))
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Failed to authorize user"))

			return
		}

		accessCookie, err := r.Cookie("access-token")
		if err == nil {
			req.AddCookie(accessCookie)
			payloadBody := new(bytes.Buffer)
			if err = json.NewEncoder(payloadBody).Encode(accessCookie.Value); err != nil {
				zap.L().Error("Failed to encode access cookie", zap.Error(err))
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte("Failed to authorize user"))

				return
			}
			req.Body = io.NopCloser(payloadBody)
		}

		refreshCookie, err := r.Cookie("refresh-token")
		if err == nil {
			req.AddCookie(refreshCookie)
		}

		resp, err := authValidate(h.Client, req)
		if err != nil {
			zap.L().Error("Failed to validate user", zap.Error(err))
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Failed to authorize user"))
			w.Write([]byte(err.Error()))

			return
		}

		var user models.User

		err = json.NewDecoder(resp.Body).Decode(&user)
		if err != nil {
			zap.L().Error("Failed to decode user", zap.Error(err))
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Failed to authorize user"))
			w.Write([]byte(err.Error()))

			return
		}

		ctx := context.WithValue(
			r.Context(),
			UsernameKey,
			user,
		)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func authValidate(c HttpClient, req *http.Request) (*http.Response, error) {
	resp, err := c.Do(req)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

func (h *Handler) ReceiversCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var receiversList []*models.Receiver
		var err error

		user, ok := GetUser(r.Context())
		if !ok {
			zap.L().Error("Failed to get user from context")
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Failed to get user"))

			return
		}
		if user_id := chi.URLParam(r, "user_id"); user_id != "" {
			if user.Username == user_id {
				receiversList, err = h.Receiversdb.GetReceiversByUser(user_id)
			} else {
				return
			}
		} else {
			return
		}
		if err != nil {
			return
		}

		ctx := context.WithValue(r.Context(), ReceiversListKey, receiversList)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (h *Handler) ReceiverCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var receiver *models.Receiver
		var err error

		user, ok := GetUser(r.Context())
		if !ok {
			zap.L().Error("Failed to get user from context")
			return
		}

		if user_id := chi.URLParam(r, "user_id"); user_id != "" {
			if user.Username == user_id {
				if receiver_id := chi.URLParam(r, "receiver_id"); receiver_id != "" {
					receiver, err = h.Receiversdb.GetReceiver(user_id)
				} else {
					return
				}
			} else {
				return
			}
		} else {
			return
		}
		if err != nil {
			return
		}

		ctx := context.WithValue(r.Context(), ReceiverKey, receiver)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
