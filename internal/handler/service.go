package handler

import (
	"encoding/json"
	"html/template"
	"io/ioutil"
	"net/http"
	"text/template/parse"

	"go.uber.org/zap"
)

//@Summary Handle I
//@Tags endpoints
//@Description Endpoint I
//@ID handlei
//@Accept json
//@Produce json
//@Failure 400,404 {string} string "bad request"
//@Failure 500 {string} string "server error"
//@Failure default {string} string "other problem"
//@Router /profile/i [get]
func (h *Handler) handleI(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	username, ok := GetUser(ctx)
	if !ok {
		zap.L().Error("Failed to get user from context")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to get user"))

		return
	}

	w.Header().Set("Content-Type", "application/json")

	if err := json.NewEncoder(w).Encode(username); err != nil {
		zap.L().Error("Failed to encode user", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to get user"))

		return
	}

	w.WriteHeader(http.StatusOK)
}

//@Summary ListUserResivers
//@Tags receiver
//@Description Get user list
//@ID userlist
//@Accept json
//@Produce json
//@Failure 400,404 {string} string "bad request"
//@Failure 500 {string} string "server error"
//@Failure default {string} string "other problem"
//@Router /profile [get]
func (h *Handler) ListUserReceivers(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	receiversList, ok := GetReceiverList(ctx)
	if !ok {
		zap.L().Error("Failed to get receivers list from context")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to get receivers list"))

		return
	}

	w.Header().Set("Content-Type", "application/json")

	if err := json.NewEncoder(w).Encode(receiversList); err != nil {
		zap.L().Error("Failed to encode receivers list", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to get receivers list"))

		return
	}

	w.WriteHeader(http.StatusOK)
}

//@Summary Create Receiver
//@Tags receiver
//@Description Create receiver
//@ID create-receiver
//@Accept json
//@Produce json
//@Failure 400,404 {string} string "bad request"
//@Failure 500 {string} string "server error"
//@Failure default {string} string "other problem"
//@Router /profile [post]
func (h *Handler) CreateReceiver(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	receiver, ok := GetReceiver(ctx)
	if !ok {
		zap.L().Error("Failed to get receiver from context")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to create receiver"))

		return
	}

	err := h.Receiversdb.NewReceiver(receiver)
	if err != nil {
		zap.L().Error("Failed to create receiver", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to create receiver"))

		return
	}

	w.WriteHeader(http.StatusOK)
}

//@Summary Get Receiver
//@Tags receiver
//@Description Get receiver
//@ID get-receiver
//@Accept json
//@Produce json
//@Failure 400,404 {string} string "bad request"
//@Failure 500 {string} string "server error"
//@Failure default {string} string "other problem"
//@Router /profile [get]
func (h *Handler) GetReceiver(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	user, ok := GetUser(ctx)
	if !ok {
		zap.L().Error("Failed to get user from context")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to get receiver"))

		return
	}

	receiver, err := h.Receiversdb.GetReceiver(user.Username)
	if err != nil {
		zap.L().Error("Failed to get receiver", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to get receiver"))

		return
	}

	if err := json.NewEncoder(w).Encode(receiver); err != nil {
		zap.L().Error("Failed to encode receiver", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to get receiver"))

		return
	}

	w.WriteHeader(http.StatusOK)
}

//@Summary Update Receiver
//@Tags receiver
//@Description Update receiver
//@ID update-receiver
//@Accept json
//@Produce json
//@Failure 400,404 {string} string "bad request"
//@Failure 500 {string} string "server error"
//@Failure default {string} string "other problem"
//@Router /profile [put]
func (h *Handler) UpdateReceiver(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	receiver, ok := GetReceiver(ctx)
	if !ok {
		zap.L().Error("Failed to get user from context")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to update receiver"))

		return
	}

	err := h.Receiversdb.UpdateReceiver(receiver)
	if err != nil {
		zap.L().Error("Failed to update receiver", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to update receiver"))

		return
	}

	w.WriteHeader(http.StatusOK)
}

//@Summary Delete Receiver
//@Tags receiver
//@Description Delete receiver
//@ID delete-receiver
//@Accept json
//@Produce json
//@Failure 400,404 {string} string "bad request"
//@Failure 500 {string} string "server error"
//@Failure default {string} string "other problem"
//@Router /profile [post]
func (h *Handler) DeleteReceiver(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	receiver, ok := GetReceiver(ctx)
	if !ok {
		zap.L().Error("Failed to get receiver from context")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to delete receiver"))

		return
	}

	err := h.Receiversdb.RemoveReceiver(receiver)
	if err != nil {
		zap.L().Error("Failed to delete receiver", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to delete receiver"))

		return
	}

	w.WriteHeader(http.StatusOK)
}

//@Summary UploadTemplate
//@Tags templates
//@Description Upload Template
//@ID uploadtemplate
//@Accept json
//@Produce json
//@Failure 400,404 {string} string "bad request"
//@Failure 500 {string} string "server error"
//@Failure default {string} string "other problem"
//@Router /profile/upload_template [post]
func (h *Handler) UploadTemplate(w http.ResponseWriter, r *http.Request) {
	r.ParseMultipartForm(10 << 20) // max upload 10MB file

	templateFile, _, err := r.FormFile("template") // no need for header
	if err != nil {
		zap.L().Error("Failed to upload template", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to upload file"))

		return
	}

	defer templateFile.Close()

	fileBytes, err := ioutil.ReadAll(templateFile)
	if err != nil {
		zap.L().Error("Failed to read template", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to upload file"))

		return
	}
	tmpl := template.Must(template.New("template").Parse(string(fileBytes)))

	var parameters []string
	for _, node := range tmpl.Tree.Root.Nodes {
		if node.Type() == parse.NodeAction {
			parameter := node.(*parse.ActionNode)

			for _, p := range parameter.Pipe.Cmds {
				parameters = append(parameters, p.String())
			}
		}
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(parameters); err != nil {
		zap.L().Error("Failed to encode template", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to upload file"))

		return
	}

	w.WriteHeader(http.StatusOK)
}
