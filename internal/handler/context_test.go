package handler

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	mock_handler "gitlab.com/RobertArktes/profile/internal/handler/mocks"
	"gitlab.com/RobertArktes/profile/internal/infrastructure/postgre"
	"gitlab.com/RobertArktes/profile/internal/models"
	"go.uber.org/zap"
)

func Test_GetUser(t *testing.T) {
	User1 := models.User{
		Username: "username",
	}
	User2 := models.User{}

	ctx := context.Background()

	testCases := []struct {
		name   string
		ctx    context.Context
		ok     bool
		result string
	}{
		{
			name:   "correct variant",
			ctx:    context.WithValue(ctx, UsernameKey, User1),
			ok:     true,
			result: "username",
		},
		{
			name: "empty username",
			ctx:  context.WithValue(ctx, UsernameKey, User2),
			ok:   true,
		},
		{
			name: "empty context",
			ctx:  ctx,
			ok:   false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			result, ok := GetUser(tc.ctx)

			assert.Equal(t, tc.ok, ok)
			assert.Equal(t, result, result)
		})
	}
}

func Test_GetReceiverList(t *testing.T) {
	var receiversListEmpty []*models.Receiver
	receiversList := append(receiversListEmpty, &models.Receiver{
		UserId:     "Kenobi",
		ReceiverId: "Jedi council",
	})
	receiversList = append(receiversList, &models.Receiver{
		UserId:     "Yoda master",
		ReceiverId: "General Kenobi",
	})

	ctx := context.Background()

	testCases := []struct {
		name   string
		ctx    context.Context
		ok     bool
		result []*models.Receiver
	}{
		{
			name:   "correct variant",
			ctx:    context.WithValue(ctx, ReceiversListKey, receiversList),
			ok:     true,
			result: receiversList,
		},
		{
			name:   "empty receiver",
			ctx:    context.WithValue(ctx, ReceiversListKey, receiversListEmpty),
			ok:     true,
			result: receiversListEmpty,
		},
		{
			name: "empty context",
			ctx:  ctx,
			ok:   false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			result, ok := GetReceiverList(tc.ctx)

			assert.Equal(t, tc.ok, ok)
			assert.Equal(t, result, result)
		})
	}
}

func Test_GetReceiver(t *testing.T) {
	var receiverEmpty models.Receiver
	receiver := models.Receiver{
		UserId:     "Kenobi",
		ReceiverId: "Jedi council",
	}

	ctx := context.Background()

	testCases := []struct {
		name   string
		ctx    context.Context
		ok     bool
		result models.Receiver
	}{
		{
			name:   "correct variant",
			ctx:    context.WithValue(ctx, ReceiverKey, receiver),
			ok:     true,
			result: receiver,
		},
		{
			name:   "empty receiver",
			ctx:    context.WithValue(ctx, ReceiverKey, receiverEmpty),
			ok:     true,
			result: receiverEmpty,
		},
		{
			name: "empty context",
			ctx:  ctx,
			ok:   false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			result, ok := GetReceiver(tc.ctx)

			assert.Equal(t, tc.ok, ok)
			assert.Equal(t, result, result)
		})
	}
}

func Test_userContext(t *testing.T) {
	type mockBehavior func(m *mock_handler.MockHttpClient, req *http.Request)

	handlerToTest := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user, ok := GetUser(r.Context())
		if !ok {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		_, err := w.Write([]byte(user.Username))

		assert.Nil(t, err)
	})

	User := models.User{
		Username: "Solomon",
	}

	body, err := json.Marshal(User)
	if err != nil {
		zap.L().Error("Failed to marshal body - ", zap.Error(err))
	}

	mockResponse := &http.Response{
		Body: ioutil.NopCloser(bytes.NewBuffer(body)),
	}

	testCases := []struct {
		name               string
		expectedStatusCode int
		mockBehavior       mockBehavior
	}{
		{
			name:               "correct variant",
			expectedStatusCode: 200,
			mockBehavior: func(m *mock_handler.MockHttpClient, req *http.Request) {
				m.EXPECT().Do(gomock.Any()).Return(mockResponse, nil)
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			c := gomock.NewController(t)
			defer c.Finish()

			m := mock_handler.NewMockHttpClient(c)

			repos := &postgre.Repository{}
			handler := NewHandler(repos, m)

			res := httptest.NewRecorder()
			req, err := http.NewRequest("", "", nil)
			if err != nil {
				zap.L().Fatal("Failed to create request ", zap.Error(err))
			}
			tc.mockBehavior(m, req)

			handler.userContext(handlerToTest).ServeHTTP(res, req)

			fmt.Println(res.Body)
			assert.Equal(t, tc.expectedStatusCode, res.Code)
		})
	}
}
