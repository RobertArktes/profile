package handler

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "gitlab.com/RobertArktes/profile/docs"
	"gitlab.com/RobertArktes/profile/internal/infrastructure/postgre"
)

type Handler struct {
	Receiversdb *postgre.Repository
	Client      HttpClient
}

func NewHandler(repos *postgre.Repository, cli HttpClient) *Handler {
	return &Handler{
		Receiversdb: repos,
		Client:      cli,
	}
}

type HttpClient interface {
	Do(req *http.Request) (*http.Response, error)
}

func (h *Handler) InitRoutes() *chi.Mux {
	router := chi.NewRouter()

	router.Route("/profile", func(r chi.Router) {
		router.Get("/swagger/*", httpSwagger.Handler(
			httpSwagger.URL("http://localhost:8081/profile/swagger/doc.json"), // The url pointing to API definition
		))

		r.Use(h.userContext)
		r.Get("/i", h.handleI)
		r.Route("/receivers/{user_id}", func(r chi.Router) {
			r.With(h.ReceiversCtx).Get("/", h.ListUserReceivers)
			r.Post("/", h.CreateReceiver)                                    // POST /profile/receivers/{user_id} - creates receiver for user_id
			r.With(h.ReceiverCtx).Get("/{receiver_id}", h.GetReceiver)       // GET /profile/receivers/{user_id}/{receiver_id} - get receiver by id
			r.With(h.ReceiverCtx).Put("/{receiver_id}", h.UpdateReceiver)    // PUT /profile/receivers/{user_id}/{receiver_id} - update receiver
			r.With(h.ReceiverCtx).Delete("/{receiver_id}", h.DeleteReceiver) // DELETE /profile/receivers/{user_id}/{receiver_id}
		})
		router.Post("/upload_template", h.UploadTemplate)
	})

	return router
}
