package postgre

import (
	"context"

	"github.com/jackc/pgx/v4"
	log "github.com/sirupsen/logrus"
	"gitlab.com/RobertArktes/profile/internal/models"
)

//go:generate mockgen -source=db.go -destination=mocks/mock.go

func NewPostgreDB(uri string) (*pgx.Conn, error) {
	conn, err := pgx.Connect(context.Background(), uri)

	return conn, err
}

type Repository struct {
	Service
}

type Service interface {
	NewReceiver(rec models.Receiver) error
	GetReceiver(userId string) (*models.Receiver, error)
	UpdateReceiver(rec models.Receiver) error
	RemoveReceiver(rec models.Receiver) error
	GetReceiversByUser(userId string) ([]*models.Receiver, error)
}

func NewRepository(db *pgx.Conn) *Repository {
	return &Repository{
		Service: NewReceiversdb(db),
	}
}

type Receiversdb struct {
	db *pgx.Conn
}

func NewReceiversdb(db *pgx.Conn) *Receiversdb {
	return &Receiversdb{
		db: db,
	}
}

func (r *Receiversdb) NewReceiver(rec models.Receiver) error {
	ctx := context.Background()

	tx, err := r.db.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)

	batch := new(pgx.Batch)

	batch.Queue(`INSERT INTO receiversdb (userid, receiver) VALUES ($1, $2);`, rec.UserId, rec.ReceiverId)

	res := tx.SendBatch(ctx, batch)

	err = res.Close()
	if err != nil {
		return err
	}

	return nil
}

func (r *Receiversdb) GetReceiver(userId string) (*models.Receiver, error) {
	var receiver models.Receiver

	receiver.UserId = userId
	query := `SELECT receiver FROM receiversdb where userid=$2;`
	err := r.db.QueryRow(context.Background(), query, userId).Scan(&receiver.ReceiverId)
	if err != nil {
		log.Error("Failed to get receiver")
	}
	return &receiver, err
}

func (r *Receiversdb) UpdateReceiver(rec models.Receiver) error {
	ctx := context.Background()

	tx, err := r.db.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)

	batch := new(pgx.Batch)

	batch.Queue(`INSERT INTO receiversdb (userid, receiver) VALUES ($1, $2);`, rec.UserId, rec.ReceiverId)

	res := tx.SendBatch(ctx, batch)

	err = res.Close()
	if err != nil {
		return err
	}

	return nil
}

func (r *Receiversdb) RemoveReceiver(rec models.Receiver) error {
	ctx := context.Background()

	tx, err := r.db.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)

	batch := new(pgx.Batch)

	batch.Queue(`DELETE FROM receiversdb WHERE userid = $1 AND receiver = $2;`, rec.UserId, rec.ReceiverId)

	res := tx.SendBatch(ctx, batch)

	err = res.Close()
	if err != nil {
		return err
	}

	return nil
}

func (r *Receiversdb) GetReceiversByUser(userId string) ([]*models.Receiver, error) {
	var receiversList []*models.Receiver

	query := `SELECT receiver FROM receiversdb WHERE userid = $2;`
	rows, err := r.db.Query(context.Background(), query, userId)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var r models.Receiver
		err = rows.Scan(
			&r.UserId,
			&r.ReceiverId,
		)
		if err != nil {
			return nil, err
		}
		receiversList = append(receiversList, &r)
	}
	return receiversList, err
}
