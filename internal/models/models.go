package models

type User struct {
	Username string `json:"username"`
	//UserId   string `json:"userid"`
}

type Receiver struct {
	UserId     string `json:"user_id"`     // the owner
	ReceiverId string `json:"receiver_id"` // receiver from owner
}

type ReceiverRequest struct {
	*Receiver
}
