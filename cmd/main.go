package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/RobertArktes/profile/internal/handler"
	"gitlab.com/RobertArktes/profile/internal/infrastructure/postgre"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

//@title Profile service Api
//@version 1.0
//@description Api server for profile application

//@host localhost:8081/profile
//@BasePath /
func main() {
	logger := initLogger()

	defer func() {
		if err := logger.Sync(); err != nil {
			zap.L().Error("error syncing logger", zap.Error(err))
		}
	}()

	if err := InitConfig(); err != nil {
		zap.L().Fatal("error initializing config: %s", zap.Error(err))
	}

	zap.L().Info("config initialized")

	db, err := postgre.NewPostgreDB(viper.GetString("db.uri"))
	if err != nil {
		zap.L().Fatal("failed to initializing database: %s", zap.Error(err))
	}

	pdb := postgre.NewRepository(db)
	cli := &http.Client{}

	handlers := handler.NewHandler(pdb, cli)
	srv := new(Server)

	go func() {
		if err := srv.Run(viper.GetString("server.port"), handlers.InitRoutes()); err != nil &&
			err != http.ErrServerClosed {
			zap.L().Fatal("error running server: %s", zap.Error(err))
		}
	}()

	zap.L().Info("server started")

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	if err := srv.Shutdown(context.Background()); err != nil {
		zap.L().Fatal("error shutting down server", zap.Error(err))
	}

	zap.L().Info("server shut down")

	if err = db.Close(context.Background()); err != nil {
		zap.L().Fatal("error close database", zap.Error(err))
	}
}

func InitConfig() error {
	viper.AddConfigPath("cmd/")
	viper.SetConfigName("config")

	return viper.ReadInConfig()
}

type Server struct {
	httpServer *http.Server
}

func (s *Server) Run(port string, handler http.Handler) error {
	s.httpServer = &http.Server{
		Addr:           ":" + port,
		Handler:        handler,
		MaxHeaderBytes: 1 << 20,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   20 * time.Second,
	}

	return s.httpServer.ListenAndServe()
}

func (s *Server) Shutdown(ctx context.Context) error {
	return s.httpServer.Shutdown(ctx)
}

func initLogger() *zap.Logger {
	var logger *zap.Logger

	var err error

	zapConfig := zap.NewProductionConfig()
	zapConfig.EncoderConfig.TimeKey = "timestamp"
	zapConfig.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder

	if logger, err = zapConfig.Build(); err != nil {
		zap.L().Fatal("Error building zap logger:", zap.Error(err))
	}

	zap.ReplaceGlobals(logger)

	return logger
}
